# How to Run
``` 
npm install
npm start
```

# Technology used
* Automation tools: `webpack`, `babel`
* Routing: `react-router`
* State management: `redux`, `redux-thunk`
* Style: `scss`, `bootstrap`

# Knowledge used
* List - detail
* Pagination
* Slider