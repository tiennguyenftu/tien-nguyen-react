import merge from 'deepmerge';
import {
  FETCH_MOVIES_SUCCESS,
  FETCH_MOVIE_BY_ID_SUCCESS
} from '../constants/movieConstants';

const initialState = {
  movies: {
    total_pages: 0,
    total_results: 0,
    results: {
      popular: [],
      top_rated: [],
      upcoming: [],
      now_playing: []
    }
  },
  currentMovie: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MOVIES_SUCCESS:
      console.log('state', merge(state, action.movies));
      return merge(state, action.movies);
    case FETCH_MOVIE_BY_ID_SUCCESS:
      return {
        ...state,
        ...action.currentMovie
      };
    default:
      return state;
  }
}