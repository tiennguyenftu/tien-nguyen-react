import axios from 'axios';
import {
  FETCH_MOVIES_SUCCESS,
  FETCH_MOVIE_BY_ID_SUCCESS
} from '../constants/movieConstants';
import {baseUrl, apiKey} from '../../helper';

export const fetchMoviesSuccess = (movies) => {
  return {
    type: FETCH_MOVIES_SUCCESS,
    movies
  }
};

export const fetchMovies = (sorting, pageNum) => {
  return (dispatch) => {
    if (!pageNum) {
      pageNum = 1;
    }

    const apiUrl = `${baseUrl}/movie/${sorting}?api_key=${apiKey}&language=en-US&page=${pageNum}`;
    return axios.get(apiUrl)
      .then(res => {
        dispatch(fetchMoviesSuccess({
          movies: {
            total_pages: res.data.total_pages,
            total_results: res.data.total_results,
            results: {
              [sorting]: res.data.results
            }
          }
        }))
      })
      .catch(e => {
        throw(e);
      })
  }
};

export const fetchMovieByIdSuccess = (currentMovie) => {
  return {
    type: FETCH_MOVIE_BY_ID_SUCCESS,
    currentMovie
  }
};

export const fetchMovieById = (id) => {
  return (dispatch) => {
    const apiUrl = `${baseUrl}/movie/${id}?api_key=${apiKey}`;
    return axios.get(apiUrl)
      .then(res => {
        dispatch(fetchMovieByIdSuccess({
          currentMovie: res.data
        }))
      })
      .catch(e => {
        throw(e);
      });
  }
};