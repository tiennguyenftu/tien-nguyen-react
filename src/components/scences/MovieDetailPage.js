import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as movieActions from '../../redux/actions/movieActions';
import {resourceUrl} from '../../helper';

class MovieDetailPage extends Component {
  componentDidMount() {
    this.props.fetchMovieById(this.props.params.movie_id);
  }

  _renderGenres(genres) {
    if (genres && genres.length) {
      const genresStr = genres.map(genre => {
        return genre.name;
      }).join(', ');
      return <p>{genresStr}</p>
    }
    return null;
  }

  render() {
    const {currentMovie} = this.props;
    console.log('currentMovie', currentMovie);
    return (
      <div className="movie-detail-page">
        <img
          className="backdrop-img"
          src={`${resourceUrl}/w1000/${currentMovie.backdrop_path}`}
          alt={currentMovie.title}
          onError={e => e.target.src = '/public/images/default-backdrop.png'}
        />
        <h1>{currentMovie.title}</h1>
        <p><strong>Release Date</strong></p>
        <p>{currentMovie.release_date}</p>
        <p><strong>Overview</strong></p>
        <p>{currentMovie.overview}</p>
        <p><strong>Genres</strong></p>
        {this._renderGenres(currentMovie.genres)}
        <p><strong>Vote Average</strong></p>
        <p>
          {currentMovie.vote_average} <span className="glyphicon glyphicon-star"></span> ({currentMovie.vote_count} votes)
        </p>
        <p><strong>Budget</strong></p>
        <p>${currentMovie.budget}</p>
        <p><strong>Revenue</strong></p>
        <p>${currentMovie.revenue}</p>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentMovie: state.movies.currentMovie
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMovieById: (id) => dispatch(movieActions.fetchMovieById(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetailPage);