import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as movieActions from '../../redux/actions/movieActions';
import MovieThumbnail from '../common/MovieThumbnail';
import Pagination from '../common/Pagination';
import {sorting} from '../../helper';

class NowPlayingPage extends Component {
  constructor(props) {
    super(props);

    this._renderMovies = this._renderMovies.bind(this);
  }

  componentDidMount() {
    this.props.fetchMovies(sorting.now_playing, this.props.location.query.p);
  }

  _renderMovies() {
    return this.props.movies.map(movie => <MovieThumbnail key={movie.id} movie={movie}/>);
  }

  render() {
    const {total_pages, fetchMovies, location} = this.props;
    return (
      <div className="row">
        {this._renderMovies()}
        <div className="row text-center" id="react-paginate">
          <Pagination pageCount={total_pages} path={location.pathname} fetchMovies={fetchMovies} sorting={sorting.now_playing}/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    movies: state.movies.movies.results.now_playing,
    total_pages: state.movies.movies.total_pages
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMovies: (sorting, pageNum) => dispatch(movieActions.fetchMovies(sorting, pageNum))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(NowPlayingPage);