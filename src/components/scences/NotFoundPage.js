import React, {Component} from 'react';

class NotFound extends Component {
  render() {
    return (
      <p className="text-center">404 - Not Found.</p>
    )
  }
}

export default NotFound;