import React, {Component} from 'react';
import {connect} from 'react-redux';
import Slider from 'react-slick';
import * as movieActions from '../../redux/actions/movieActions';
import MovieCarouselItem from '../common/MovieCarouselItem';
import {sorting} from '../../helper';

class Home extends Component {
  constructor(props) {
    super(props);

    this._renderMovies = this._renderMovies.bind(this);
  }

  componentDidMount() {
    this.props.fetchMovies(sorting.popular);
    this.props.fetchMovies(sorting.top_rated);
    this.props.fetchMovies(sorting.upcoming);
    this.props.fetchMovies(sorting.now_playing);
  }

  _renderMovies(movies) {
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4
    };

    if (movies && movies.length) {
      return (
        <Slider {...settings}>
          {movies.map(movie => (
            <div key={movie.id}>
              <MovieCarouselItem movie={movie}/>
            </div>
          ))}
        </Slider>
      )
    }
    return null;
  }

  render() {
    const {popular, top_rated, upcoming, now_playing} = this.props;

    return (
      <div className="home-page">
        <div className="row">
          <h1 className="sorting-title">Popular</h1>
          {this._renderMovies(popular)}
        </div>
        <div className="row">
          <h1 className="sorting-title">Top Rated</h1>
          {this._renderMovies(top_rated)}
        </div>
        <div className="row">
          <h1 className="sorting-title">Upcoming</h1>
          {this._renderMovies(upcoming)}
        </div>
        <div className="row">
          <h1 className="sorting-title">Now Playing</h1>
          {this._renderMovies(now_playing)}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    popular: state.movies.movies.results.popular,
    top_rated: state.movies.movies.results.top_rated,
    upcoming: state.movies.movies.results.upcoming,
    now_playing: state.movies.movies.results.now_playing
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMovies: (sorting, pageNum) => dispatch(movieActions.fetchMovies(sorting, pageNum))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);