import React from 'react';
import MovieThumbnail from '../common/MovieThumbnail';

const MovieList = ({movies}) => {
  return movies.map(movie => <MovieThumbnail key={movie.id} movie={movie}/>);
};

export default MovieList;
