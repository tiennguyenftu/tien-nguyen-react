import React, {Component} from 'react';
import {Link} from 'react-router';

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      navCollapsed: true
    }
  }

  _onToggleNav() {
    this.setState({
      navCollapsed: !this.state.navCollapsed
    });
  }

  render() {
    const {navCollapsed} = this.state;

    return (
      <nav className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle collapsed"
              aria-expanded="false"
              onClick={this._onToggleNav.bind(this)}
            >
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <Link className="navbar-brand" to="/">
              <img src="/public/images/logo.png" alt="Logo" width={30}/> Movy
            </Link>
          </div>

          <div className={navCollapsed ? "collapse navbar-collapse" : "navbar-collapse"}>
            <ul className="nav navbar-nav navbar-right">
              {/*<li className="dropdown">*/}
                {/*<a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span className="caret"></span></a>*/}
                {/*<ul className="dropdown-menu">*/}
                  {/*<li><a href="#">Action</a></li>*/}
                  {/*<li><a href="#">Another action</a></li>*/}
                  {/*<li><a href="#">Something else here</a></li>*/}
                  {/*<li role="separator" className="divider"></li>*/}
                  {/*<li><a href="#">Separated link</a></li>*/}
                {/*</ul>*/}
              {/*</li>*/}
              <li><Link to="/movies/popular">Popular</Link></li>
              <li><Link to="/movies/top-rated">Top Rated</Link></li>
              <li><Link to="/movies/upcoming">Upcoming</Link></li>
              <li><Link to="/movies/now-playing">Now Playing</Link></li>
            </ul>
          </div>
        </div>
      </nav>
    )
  }
}

export default NavBar;