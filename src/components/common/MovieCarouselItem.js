import React from 'react';
import {Link} from 'react-router';
import {resourceUrl} from '../../helper';

const MovieCarouselItem = ({movie}) => {
  return (
    <Link to={`/movies/${movie.id}`} className="movie-thumbnail-link">
      <div className="thumbnail movie-thumbnail">
        <img
          className="movie-thumbnail-img"
          src={`${resourceUrl}/w300_and_h450_bestv2/${movie.poster_path}`}
          alt={movie.title}
          onError={e => e.target.src = '/public/images/default-thumbnail.png'}
        />
        <div className="text-center movie-thumbnail-title">
          <h3>{movie.title}</h3>
        </div>
      </div>
    </Link>
  )
};


export default MovieCarouselItem;