import React from 'react';

const Footer = () => {
  return (
    <footer className="text-center">
      <p>Copyright &copy; 2017 by Tien Nguyen. All rights reserved.</p>
    </footer>
  )
};

export default Footer;