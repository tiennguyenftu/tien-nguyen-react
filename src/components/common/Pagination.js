import React from 'react';
import ReactPaginate from 'react-paginate';
import {browserHistory} from 'react-router';

const Pagination = ({sorting, pageCount, path, fetchMovies}) => {
  return (
    <div className="row text-center" id="react-paginate">
      <ReactPaginate
        previousLabel={"previous"}
        nextLabel={"next"}
        breakLabel={<a href="">...</a>}
        breakClassName={"break-me"}
        pageCount={pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={(data) => {
          browserHistory.push(`${path}?p=${data.selected + 1}`);
          fetchMovies(sorting, data.selected + 1);
        }}
        containerClassName={"pagination"}
        subContainerClassName={"pages pagination"}
        activeClassName={"active"}
      />
    </div>
  )
};

export default Pagination;