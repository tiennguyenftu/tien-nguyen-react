import React from 'react';
import NavBar from './common/NavBar';
import Footer from './common/Footer';

const App = ({children}) => {
  return (
    <div>
      <NavBar/>
      <div className="container">
        {children}
      </div>
      <Footer/>
    </div>
  )
};

export default App;