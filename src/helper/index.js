module.exports = {
  apiKey: 'eb84fb9cfa7e60d20b49bdc7238f66b0',
  baseUrl: 'https://api.themoviedb.org/3',
  resourceUrl: 'https://image.tmdb.org/t/p',
  sorting: {
    popular: 'popular',
    top_rated: 'top_rated',
    upcoming: 'upcoming',
    now_playing: 'now_playing'
  }
};