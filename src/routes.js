import React from 'react';
import {Route, IndexRoute} from 'react-router';

import App from './components/App';
import Home from './components/scences/HomePage';
import NowPlaying from './components/scences/NowPlayingPage';
import Popular from './components/scences/PopularPage';
import TopRated from './components/scences/TopRatedPage';
import Upcoming from './components/scences/UpcomingPage';
import MovieDetail from './components/scences/MovieDetailPage';
import NotFound from './components/scences/NotFoundPage';

export default (
  <Route path="/" components={App}>
    <IndexRoute components={Home}/>
    <Route path="/movies/now-playing" components={NowPlaying}/>
    <Route path="/movies/popular" components={Popular}/>
    <Route path="/movies/top-rated" components={TopRated}/>
    <Route path="/movies/upcoming" components={Upcoming}/>
    <Route path="movies/:movie_id" components={MovieDetail}/>
    <Route path="*" components={NotFound}/>
  </Route>
)